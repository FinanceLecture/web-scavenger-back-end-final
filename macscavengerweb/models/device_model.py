from .device_model_loading import DeviceConfig
from .. import db

class Device(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    port = db.Column(db.Integer)
    ip_address = db.Column(db.String(20))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    added = db.Column(db.Boolean, default=False)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def serialize(self):
        return {
            "id": self.id,
            "name": self.name,
            "port": self.port,
            "ip_address": self.ip_address,
            "added": self.added,
        }

    def map_to_config(self):
        return DeviceConfig(self.ip_address, self.name, self.port)

    @classmethod
    def find_by_IP(cls, ip_address):
        return cls.query.filter_by(ip_address=ip_address).first()

class Event(db.Model):
    __tablename__ = 'event'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Integer)
    device = db.Column(db.String(40))
    time = db.Column(db.DateTime)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def serialize(self):
        return {
            "id": self.id,
            "title": self.title,
            "device": self.device,
            "time": self.time,
        }

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
