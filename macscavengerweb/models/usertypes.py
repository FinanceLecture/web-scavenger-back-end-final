from enum import Enum


class UserTypes(Enum):
    admin = True,
    guest = False,


class EventTypes(Enum):
    started = 1,
    stopped = 2,
    added = 3,
    analyzed = 4,
    found = 5,
    warning = 6,
    error = 7,
