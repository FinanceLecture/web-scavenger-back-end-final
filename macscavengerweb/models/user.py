from .. import db
from passlib.hash import pbkdf2_sha256 as sha256
from cryptography.fernet import Fernet
from macscavengerweb import app

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    role = db.Column(db.String(50))
    username = db.Column(db.String(50), unique=True, nullable=False)
    password = db.Column(db.String(50), nullable=False)
    accepted = db.Column(db.Boolean, default=False)
    devices = db.relationship('Device', backref='user')
    history = db.relationship('Event', backref='historian')
    running = db.Column(db.Boolean, default=False)


    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def find_by_username(cls, username):
        return cls.query.filter_by(username=username).first()

    @staticmethod
    def encrypt(message: bytes, key: bytes) -> bytes:
        return Fernet(key).encrypt(message)

    @staticmethod
    def decrypt(token: bytes, key: bytes) -> bytes:
        return Fernet(key).decrypt(token)

    @staticmethod
    def generate_hash(password):
        return sha256.hash(password)

    @staticmethod
    def verify_hash(password, hash):
        return sha256.verify(password, hash)

    def get_history(self):
        return self.history