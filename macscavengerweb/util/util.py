import yaml
import os

def noop(self, *args, **kw):
    pass

def write_device_file(list_of_devices, file_path_mac):
    file_path = os.path.join(file_path_mac, 'config','config.yaml')

    yaml.emitter.Emitter.process_tag = noop

    with open(file_path, 'w') as file:
        yaml.dump(list_of_devices, file)

