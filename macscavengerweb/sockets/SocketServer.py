import socket
import pickle
import time


def get_device_info():
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    working = True

    #TODO replace it with IP adress of production server

    host = '########'
    port = 2000

    server_socket.settimeout(60)
    server_socket.bind((host, port))
    server_socket.listen(5)
    print("Server is listening")

    timeout = time.time() + 60   # 1 minute from now
    data=None

    while True:
        try:
            client, address = server_socket.accept()
            print("Connection with Client"+ str(address) +"was established")
        except socket.timeout:
            print("timeout")
            working = False
            break

        msg = client.recv(1024)

        data = pickle.loads(msg)
        print('received data')

        if address[0]:
            client.send(bytes("OK", "utf-8"))
            client.close()
            data['ip_address'] = address[0]
            break
        if time.time() > timeout:
            print("timeout")
            working=False
            break

    return working, data
