from datetime import timedelta
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_jwt_extended import JWTManager
from cryptography.fernet import Fernet

import redis
#from pathlib import Path
#from dotenv import load_dotenv

#env_path = Path('../') / '.env'
#load_dotenv(dotenv_path=env_path)
import os

db = SQLAlchemy()
app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = 'Burkhard'
app.config['JWT_SECRET_KEY'] = 'Burkhard'
app.config['JWT_BLACKLIST_ENABLED'] = True
app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = timedelta(minutes=30)
app.config['JWT_REFRESH_TOKEN_EXPIRES'] = timedelta(hours=1)
app.config['DATABASE_SECRET'] = Fernet.generate_key()

jwtLib = JWTManager(app)
blacklist = set()

def create_app():

    db.init_app(app)

    @app.route('/api')
    def hello_world():
        return "Displays Hello World "

    from macscavengerweb.api.controller import controller
    from macscavengerweb.api.loginControl import loginControl
    from macscavengerweb.api.utils import utils
    app.register_blueprint(controller)
    app.register_blueprint(loginControl)
    app.register_blueprint(utils)

    return app
