from functools import wraps

import jwt
from flask import Blueprint, jsonify, request, abort
from flask_cors import cross_origin
from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required, get_jwt_identity, get_raw_jwt)
from ..models.usertypes import UserTypes
from macscavengerweb import db, blacklist, jwtLib
from ..models.user import User


loginControl = Blueprint('loginControl', __name__)
JWT_LIFE_SPAN = 10

def mapper(user):
    if user.role == UserTypes.guest.name:
        return {"username": user.username, "accepted": user.accepted }


def restricted():
    def decorator(func):
        @wraps(func)
        def wrapping_funct(*args, **kwargs):
            token = user_data = request.headers
            bear_token = token['Authorization'][7:]
            decoded_token = jwt.decode(bear_token, 'Burkhard')
            username = decoded_token['identity']

            user = User.find_by_username(username)

            if not user or not user.accepted:
                abort(409)
            return func(*args, **kwargs)
        return wrapping_funct
    return decorator

@jwtLib.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token['jti']
    return jti in blacklist

@loginControl.route('/api/login', methods=['POST'])
@cross_origin()
def login():
    user_data = request.get_json()

    user_to_check = User(username=user_data['username'], password=user_data['password'])

    user_from_db = User.find_by_username(user_to_check.username)

    if not user_from_db:
        return {"message": "No such User in database"}, 406

    if user_to_check.password == user_from_db.password:
        if not user_from_db.role == UserTypes.admin.name and not user_from_db.accepted:
            return { "message": "Your registration hasn't been reviewed by the administrator! Contact the administrator"}, 406

        access_token = create_access_token(identity=user_from_db.username)
        refresh_token = create_refresh_token(identity=user_from_db.username)
        return {
                   'message': "Accepted",
                   'username': user_from_db.username,
                   'role': user_from_db.role,
                   'access_token': access_token,
                   'refresh_token': refresh_token}, 202
    else:
        return {"message": "Wrong password"}, 406


@loginControl.route('/api/register', methods=['POST'])
@cross_origin()
def register():
    user_data = request.get_json()

    new_user = User(username=user_data['username'], password=user_data['password'])
    new_user.role = UserTypes.guest.name

    if User.find_by_username(new_user.username):
        message = "User with Username " + new_user.username + " already exists. Try another one"
        return {'message': message}

    new_user.save_to_db()

    return "Done", 201


@loginControl.route('/api/check_token', methods=['POST'])
@restricted()
@jwt_required
@cross_origin()
def check_token():
    return "allowed", 200


@loginControl.route('/api/check_role', methods=['GET'])
@cross_origin()
@restricted()
@jwt_required
def check_role():
    username = get_jwt_identity()

    user_from_db = User.find_by_username(username)

    if not user_from_db:
        return "Not Found", 406

    extended_rights = (user_from_db.username == UserTypes.admin.name)

    if not extended_rights:
        return {"role": user_from_db.role, "extended_rights": extended_rights, "users": []}, 200

    all_users = User.query.all()

    users_to_return = list(map(mapper, all_users))
    users_to_return = list(filter(None, users_to_return))

    devices = []
    for device in user_from_db.devices:
        devices.append(device.serialize())

    return {
                "role": user_from_db.role,
                "extended_rights": extended_rights,
                "users": users_to_return,
                "devices": devices,
                "running": user_from_db.running,
            }, 200


@loginControl.route('/api/register', methods=['PUT'])
@cross_origin()
@restricted()
@jwt_required
def update_user():
    update = request.get_json()

    admin_name = get_jwt_identity()
    admin = User.find_by_username(admin_name)
    updatable_user = User.find_by_username(update['username'])

    if not admin or admin.role != UserTypes.admin.name or not updatable_user:
        return "Conflict", 409

    updatable_user.accepted = update["accepted"]
    db.session.commit()

    return { "username": updatable_user.username, "accepted": updatable_user.accepted}, 202

@loginControl.route('/api/logout', methods=['DELETE'])
@jwt_required
def logout():
    jti = get_raw_jwt()['jti']
    blacklist.add(jti)
    return jsonify({"msg": "Successfully logged out"}), 200