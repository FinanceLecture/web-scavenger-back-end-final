from flask import Blueprint, jsonify, request
from flask_cors import cross_origin


from ..models.user import User

from macscavengerweb import db
from macscavengerweb.models.device_model import Device
from ..models.usertypes import UserTypes

utils = Blueprint('utils', __name__)

@utils.route('/api/create_admin', methods=['POST'])
@cross_origin()
def create_admin():
    admin = User(username=UserTypes.admin.name, password="burkhard", accepted=True, role=UserTypes.admin.name)

    admin.save_to_db()

    return "Created", 201

@utils.route('/api/delete_device', methods=['DELETE'])
@cross_origin()
def delete_device():
    data = request.get_json()
    ip_address = data["ip_address"]

    db.session.delete(Device.find_by_IP(ip_address))
    db.session.commit()

    return "Deleted", 200

@utils.route('/api/devices_all', methods=['GET'])
@cross_origin()
def get_all():

    devices = Device.query.all()
    result =[]
    for device in devices:
        result.append(device.serialize())

    return jsonify(result), 200

@utils.route('/api/all_users', methods=['GET'])
@cross_origin()
def get_all_users():

    users = User.query.all()
    result =[]
    for user in users:
        result.append(user.username)

    return jsonify(result), 200

@utils.route('/api/set_running', methods=['GET'])
@cross_origin()
def set_running():

    user = User.find_by_username("admin")

    user.running = False

    user.save_to_db()

    return "Done", 200