import os
from macscavengerweb.mac.MacAnalyzer import analyze
import datetime

from flask import Blueprint, jsonify, request
from flask_cors import cross_origin
from flask_jwt_extended import (jwt_required, get_jwt_identity)
from macscavengerweb import app
import subprocess
from psutil import Process

from ..models.user import User
from ..models.usertypes import EventTypes
from ..sockets.SocketServer import get_device_info

from macscavengerweb import db
from macscavengerweb.models.device_model import Device, Event
from ..util.util import write_device_file

controller = Blueprint('controller', __name__)


def history_update(user, event, device=None):
    history = user.get_history()

    oldest = datetime.datetime.now()
    oldest_event = 0
    if len(history) == 10:
        for i in range(len(history)):
            if history[i].time < oldest:
                oldest_event = i
                oldest = history[i].time

        user.history.pop(oldest_event)
        user.save_to_db()

    new_event = Event(title=event, device=device, time=datetime.datetime.now(), historian=user)

    new_event.save_to_db()

    return

@controller.route('/api/add_movie', methods=['POST'])
@cross_origin()
def add_movie():
    movie_data = request.get_json()

    new_movie = Device(ip_address = movie_data['title'], name=movie_data['rating'])

    db.session.add(new_movie)
    db.session.commit()

    return "Done", 201


@controller.route('/api/movies')
@cross_origin()
def movies():
    #query all objects from the movie table
    # -> cannot be send like this, must be simple structure like a list or dictionary
    movies_list = Device.query.all()
    movies = []

    for movie in movies_list:
        movies.append({'title': movie.title, "rating": movie.rating})

    return jsonify({'movies': movies})

#add jwt required to route
@controller.route('/api/devices', methods=['GET'])
@cross_origin()
def get_devices():
    admin = User.find_by_username('admin')
    print(admin.devices)
    return {
        'message': admin.devices[0].name,
    }

@controller.route('/api/devices', methods=['PUT'])
@cross_origin()
@jwt_required
def add_device_to_scavenger():
    update = request.get_json()
    username = get_jwt_identity()
    user = User.find_by_username(username)

    if not user:
        history_update(user, EventTypes.error.value[0])
        return "Conflict", 409

    not_Found = True

    for device in user.devices:
        if device.ip_address == update['ip_address']:
            device.added = update['added']
            not_Found = False

    if not_Found:
        history_update(user, EventTypes.error.value[0])
        return "Conflict", 409

    db.session.commit()
    devices = []

    for device in user.devices:
        devices.append(device.serialize())

    history_update(user, EventTypes.added.value[0], update['ip_address'])
    return {"devices": devices }, 202


@controller.route('/api/search_device', methods=['POST'])
@cross_origin()
@jwt_required
def search_devices():
    working, dictionary = get_device_info()
    username = get_jwt_identity()

    user = User.find_by_username(username)

    if not user:
        history_update(user, EventTypes.error.value[0])
        return { "message": "User Not Found" }, 406

    devices = []
    for device in user.devices:
        devices.append(device.serialize())

    if working:
        new_device = Device(name=dictionary['name'], ip_address=dictionary['ip_address'], port=dictionary['port'], user=user)

        # Need to use one because with find_iP_address it always exists
        if Device.query.filter(Device.ip_address == new_device.ip_address).count() <= 1:
            new_device.save_to_db()
            devices.append(new_device.serialize())
            history_update(user, EventTypes.found.value[0], new_device.ip_address)
            return {"message": "New Device Found", "devices": devices }, 200

    history_update(user, EventTypes.warning.value[0])
    return jsonify({ "message": "No new Device has been found", "devices": devices }), 202


@controller.route('/api/start_scavenger', methods=['POST'])
@cross_origin()
@jwt_required
def start_scavenger():
    username = get_jwt_identity()
    user = User.find_by_username(username)
    fileDir = os.path.dirname(os.path.realpath('__file__'))
    file_path_mac = os.path.join(fileDir, 'macscavengerweb', 'mac')

    devices = User.find_by_username(username).devices

    list_of_devices = []
    for device in devices:
        if device.added:
            d = device.map_to_config()
            list_of_devices.append(d)
    write_device_file(list_of_devices, file_path_mac)

    if len(list_of_devices) == 0:
        history_update(user, EventTypes.error.value[0])
        return { "message": "At least one monitor must be selected"}, 412

    # Might have to change it to python2 or pyhton3 depending on the webserver
    commands = "python MacScavengerShell.py"
    process = subprocess.Popen(commands, universal_newlines=True, cwd=file_path_mac, shell=True)

    app.config['MAC_PID'] = process.pid

    if not process.poll() is None:
        history_update(user, EventTypes.started.value[7])
        return {"message": "Could't start MAC-Scavenger"}, 412

    user.running = True
    user.save_to_db()
    history_update(user, EventTypes.started.value[0])

    return {"message": "MacScavenger was started"}, 200

@controller.route('/api/stop_scavenger', methods=['DELETE'])
@cross_origin()
@jwt_required
def stop_scavenger():
    pid = app.config['MAC_PID']
    parent = Process(pid)
    for child in parent.children(recursive=True):
        child.kill()
    parent.kill()

    username = get_jwt_identity()
    user = User.find_by_username(username)
    user.running = False
    user.save_to_db()
    history_update(user, EventTypes.stopped.value[0])

    return {"message": "MacScavenger was stopped"}, 200

@controller.route('/api/analyze_scavenger', methods=['GET'])
@cross_origin()
@jwt_required
def analyze_scavenger():
    username = get_jwt_identity()
    user = User.find_by_username(username)
    defaultAnalyzer = request.args.get('defaultAnalyzer')

    position_string = {'tinkerboard1':(0,0),'tinkerboard2':(2,1),'tinkerboard3':(2,5),'tinkerboard4':(0,0)}

    if defaultAnalyzer == 'false':
        try:
            # would need changes in the MacScavengerAnalyzer to fully work
            analyze(path=os.path.join(os.path.dirname(os.path.realpath('__file__')), 'macscavengerweb', 'mac', 'json_data', 'total_data.json'), positions=position_string)
            history_update(user, EventTypes.analyzed.value[0])
            return {"message": "MacScavenger data was analyzed"}, 200

        except Exception as e:
            history_update(user, EventTypes.error.value[0])
            return { "message": str(e) }, 400

    else:
        history_update(user, EventTypes.analyzed.value[0])
        summery = "Summary: \n" \
                  "Approximately 1 different recognizable devices on site that were detected by at minimum 1 APs \n" \
                  "Thereof, 0 devices were seen just once, while 1 were seen multiple times \n" \
                  "1 devices were using MAC Randomization, 0 were not applying Randomization techniques \n"
        return { "message": "MacScavenger data was analyzed", "summery": summery}, 200


@controller.route('/api/history', methods=['GET'])
@cross_origin()
@jwt_required
def get_history():
    username = get_jwt_identity()
    user = User.find_by_username(username)

    result = []
    for event in user.history:
        result.append(event.serialize())

    return { "history": result }, 200